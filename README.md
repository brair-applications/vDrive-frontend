
## Develop
#### Project setup
first run container with backend application, then check are they on the same docker network
```bash
make up
```

## Production
```bash
docker build -t vd-frontend-prod:latest .
docker run -p 80:80 --name vd-frontend-prod vd-frontend-prod:latest
# OR
make up prod
```
