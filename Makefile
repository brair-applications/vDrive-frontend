#———— Variables ————————————————————————————————————————————————————————————————————————————————————————————————————————
ALIAS 	        := vd
APP_IMAGE       := $(ALIAS)-frontend
APP_IMAGE_PROD  := $(APP_IMAGE)-prod

DOCKER_COMPOSE  := cd .docker && docker-compose
BACKEND_HOST 	:= $(ALIAS)-server-nginx
DOCKER_BACKEND_IP= $(shell docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(ALIAS)-server-nginx)
#———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
#———————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
.DEFAULT_GOAL = help
ARG := $(word 2, $(MAKECMDGOALS))
%:
	@:
help:
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | \
	awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
fix-backend-ip: ## setup backend container ip for develop env
	@sed -i "s|VUE_APP_API_URL=http.*://[^/]*|VUE_APP_API_URL=http://$(DOCKER_BACKEND_IP)|" .env.docker

## —— 🐝Vue 🐝——————————————————————————————————————————————————————————————————————————————————————————————————————————————
install: ## install dependency
	@npm install

serve: ## run server with [.env.development.local | .env.docker.local ||| default: .env.production.local]
	@if [ "${ARG}" = 'docker' ] || [ "${ARG}" = 'doc' ]; then npm run serve -- --mode docker; fi
	@if [ "${ARG}" = 'develop' ] || [ "${ARG}" = 'dev' ]; then npm run serve -- --mode development; fi
	@if [ "${ARG}" = '' ]; then npm run serve; fi

## —— 🐝Docker 🐝———————————————————————————————————————————————————————————————————————————————————————————————————————————
up: ## run app container [dev | prod]
	@if [ "${ARG}" = 'dev' ] || [ "${ARG}" = '' ]; then $(DOCKER_COMPOSE) up; fi
	@if [ "${ARG}" = 'prod' ] || [ "${ARG}" = 'p' ]; then $(DOCKER_COMPOSE) -f docker-compose.prod.yml up -d; fi

stop: ## stop container
	@$(DOCKER_COMPOSE) stop

console: ## log container app
	@if [ "${ARG}" = 'dev' ] || [ "${ARG}" = '' ]; then make fix-backend-ip && docker exec -it $(APP_IMAGE) bash; fi
	@if [ "${ARG}" = 'prod' ] || [ "${ARG}" = 'p' ]; then docker exec -it $(APP_IMAGE_PROD) bash; fi

run-prod: ## build dev image
	@docker build -t $(APP_IMAGE_PROD):latest .
	@docker run -p 80:80 --name $(APP_IMAGE_PROD) $(APP_IMAGE_PROD):latest

