const DEFAULT = [
    { icon: 'mdi-home', text: 'Home', path: '/' },
    { icon: 'mdi-youtube-subscription', text: 'Test', path: '/test' },
    { icon: 'mdi-folder-account', text: 'Drive', path: '/drive' },
];

const ADMIN = [
    { icon: 'mdi-profile', text: 'Profile', path: '/profile' },
    { icon: 'mdi-profile', text: 'Dashboard', path: '/dashboard' },
];

export default new class Navigation {
    getDefaultMenu() {
        return DEFAULT;
    }

    getAdminMenu() {
        return ADMIN;
    }
}