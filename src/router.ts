import Vue from 'vue'
import VueRouter from 'vue-router'
import HomePage from '@/view/pages/HomePage.vue'
import TestPage from '@/view/pages/TestPage.vue'
import DrivePage from '@/view/pages/DrivePage.vue'

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    component: HomePage
  },
  {
    path: '/drive',
    component: DrivePage,
    children: [
      {
        path: '/drive/:id',
        component: DrivePage
      }
    ],
  },
  {
    path: '/test',
    component: TestPage
  }
];

const router = new VueRouter({routes});

export default router;
