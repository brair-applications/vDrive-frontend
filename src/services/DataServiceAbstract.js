import axios from 'axios'

export default class DataServiceAbstract {

    endpoint = process.env.VUE_APP_API_URL + '/endpoint';

    create(data) {
        return axios.post(`${this.endpoint}`, data);
    }

    update(id, data) {
        return axios.put(`${this.endpoint}/${id}`, data);
    }

    findOne(id) {
        return axios.get(`${this.endpoint}/${id}`);
    }

    findAll() {
        return axios.get(`${this.endpoint}`);
    }

    remove(id) {
        return axios.delete(`${this.endpoint}/${id}`);
    }

    query(options) {
        return axios.get(`${this.endpoint}`, { params: options });
    }
}
