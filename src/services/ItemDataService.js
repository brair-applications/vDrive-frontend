import axios from 'axios'
import DataServiceAbstract from "./DataServiceAbstract";

export default new class DriveDataService extends DataServiceAbstract
{
    endpoint = process.env.VUE_APP_API_URL + '/item';

    updateOrderNo(data) {
        return axios.put(`${this.endpoint}/order`, data);
    }
}